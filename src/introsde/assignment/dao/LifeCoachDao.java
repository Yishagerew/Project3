package introsde.assignment.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public enum LifeCoachDao {
	 instance;
	 private static EntityManagerFactory emf;
	 /**
	  * Demonstrates the use of ThreadLocal design pattern 
	  * EntityManager is not threadsafe
	  * We can implement ThreadLocal pattern for avoiding thread safety issues.
	  * An entity manager will be checked if its alive
	  */
	 public static final ThreadLocal<EntityManager>threadlocal=new ThreadLocal<EntityManager>();
	 public static EntityManagerFactory getEntityManagerfactory()
	 {
	 if(emf==null)
	 {
	 emf=Persistence.createEntityManagerFactory("SoapHealthServices");
	 }
	 return emf;
	 }
	 public EntityManager getEntityManager()
	 {
	 EntityManager em = threadlocal.get();
	 if(em==null)
	 {
	 em=getEntityManagerfactory().createEntityManager();
	 threadlocal.set(em);
	  
	 }
	 return em;
}
	 }
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 /**
	 
	    private EntityManagerFactory emf;

	    private LifeCoachDao() {
	        if (emf!=null) {
	            emf.close();
	        }
	        emf = Persistence.createEntityManagerFactory("introsde-jpa");
	    }

	    public EntityManager createEntityManager() {
	        try {
	            return emf.createEntityManager();
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	        return null;    
	    }

	    public void closeConnections(EntityManager em) {
	        em.close();
	    }

	    public EntityTransaction getTransaction(EntityManager em) {
	        return em.getTransaction();
	    }

	    public EntityManagerFactory getEntityManagerFactory() {
	    	
	        return emf;
	    }  */

