package introsde.assignment.model;
import introsde.assignment.dao.LifeCoachDao;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
/**
* The persistent class for the "LifeStatus" database table.
*
*/
@Entity
@Table(name = "LifeStatus")
@NamedQueries({
	@NamedQuery(name="LifeStatus.findByMeasureDef",query="SELECT l FROM LifeStatus l WHERE l.measureDefinition=:measureDefinition and l.person=:person"),
	@NamedQuery(name="LifeStatus.findByMinMaxRange",query="SELECT l FROM LifeStatus l WHERE l.measureDefinition=:measureDefinition and l.measureValue > :min and l.measureValue < :max"),
	@NamedQuery(name="LifeStatus.findByMinRange",query="SELECT l FROM LifeStatus l WHERE l.measureDefinition = :measureDefinition and l.measureValue > :min"),
	//@NamedQuery(name="LifeStatus.findBymaxRange",query="SELECT l FROM LifeStatus l WHERE l.measureDefinition = :measureDefinition and l.measureValue < :max"),
	@NamedQuery(name="LifeStatus.findByMeasureName",query="SELECT l FROM LifeStatus l WHERE l.measureDefinition=:measureDefinition")
	//@NamedQuery(name="LifeStatus.findByPersonId",query="SELECT s FROM LifeStatus s WHERE s.idPerson=:idPerson")
})
@XmlRootElement(name="Measure")
@XmlAccessorType(XmlAccessType.FIELD)
public class LifeStatus implements Serializable {
private static final long serialVersionUID = 1L;
@TableGenerator(name="sqlite_lifestatus", table="sqlite_sequence",
pkColumnName="name", valueColumnName="seq",
pkColumnValue="LifeStatus")
@Id
@GeneratedValue(strategy=GenerationType.TABLE,generator="sqlite_lifestatus")
@Column(name = "mid")
private int mid;
@Column(name = "measureValue")
private int measureValue;
@Column(name="dateRegistered")
private String dateRegistered;
@OneToOne
@JoinColumn(name = "measureType", referencedColumnName = "idMeasureDef", insertable = true, updatable = true)
private MeasureDefinition measureDefinition;
@ManyToOne
@JoinColumn(name="personId",referencedColumnName="id")
@XmlTransient private Person person;
public LifeStatus() {
}

public int getMid() {
	return mid;
}

public void setMid(int mid) {
	this.mid = mid;
}

public String getDateRegistered() {
	return dateRegistered;
}

public void setDateRegistered(String dateRegistered) {
	this.dateRegistered = dateRegistered;
}

public int getMeasureValue() {
	return measureValue;
}
public void setMeasureValue(int measureValue) {
	this.measureValue = measureValue;
}
public MeasureDefinition getMeasureDefinition() {
return measureDefinition;
}
public void setMeasureDefinition(MeasureDefinition param) {
this.measureDefinition = param;
}
// we make this transient for JAXB to avoid and infinite loop on serialization
public Person getPerson() {
return person;
}
public void setPerson(Person person) {
this.person = person;
}
// Database operations
// Notice that, for this example, we create and destroy and entityManager on each operation.
// How would you change the DAO to not having to create the entity manager every time?
public static LifeStatus getLifeStatusById(int lifestatusId) {
EntityManager em = LifeCoachDao.instance.getEntityManager();
LifeStatus p = em.find(LifeStatus.class, lifestatusId);
//LifeCoachDao.instance.closeConnections(em);
return p;
}
public static List<LifeStatus> getAll() {
EntityManager em = LifeCoachDao.instance.getEntityManager();
List<LifeStatus> list = em.createNamedQuery("LifeStatus.findAll", LifeStatus.class).getResultList();
//LifeCoachDao.instance.closeConnections(em);
return list;
}
public static LifeStatus saveLifeStatus(LifeStatus p) {
EntityManager em = LifeCoachDao.instance.getEntityManager();
EntityTransaction tx = em.getTransaction();
tx.begin();
em.persist(p);
tx.commit();
//LifeCoachDao.instance.closeConnections(em);
return p;
}
public static LifeStatus updateLifeStatus(LifeStatus p) {
EntityManager em = LifeCoachDao.instance.getEntityManager();
EntityTransaction tx = em.getTransaction();
tx.begin();
p=em.merge(p);
tx.commit();
//LifeCoachDao.instance.closeConnections(em);
return p;
}
public static void removeLifeStatus(LifeStatus p) {
EntityManager em = LifeCoachDao.instance.getEntityManager();
EntityTransaction tx = em.getTransaction();
tx.begin();
p=em.merge(p);
em.remove(p);
tx.commit();
//LifeCoachDao.instance.closeConnections(em);
}
}