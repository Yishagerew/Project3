/**
 * @author Yishagerew L.
 * @DateModified Oct 28, 20145:12:38 PM
 */
package introsde.assignment.model;
import introsde.assignment.dao.LifeCoachDao;

import java.io.Serializable;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import java.util.ArrayList;
import java.util.List;

import org.joda.*;

/**
 * The persistent class for the "Person" database table.
 * 
 */
@Entity
@Table(name="Person")
@NamedQueries({
@NamedQuery(name="Person.findAll", query="SELECT p FROM Person p"),
@NamedQuery(name="person.findById",query="SELECT p FROM Person p WHERE p.id = :personId")})
@XmlRootElement(name="human")
public class Person implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@TableGenerator(name="sqlite_person", table="sqlite_sequence",
		    pkColumnName="name", valueColumnName="seq",
		    pkColumnValue="Person")
	@Id
	// For sqlite in particular, you need to use the following @GeneratedValue annotation
	// This holds also for the other tables
	// SQLITE implements auto increment ids through named sequences that are stored in a 
	// special table named "sqlite_sequence"
	@GeneratedValue(strategy=GenerationType.TABLE,generator="sqlite_person")
	@Column(name="id")
	private int id;

	@Column(name="lastname")
	private String lastname;

	@Column(name="firstname")
	private String firstname;

	@Column(name="username")
	private String username;
	
	@Column(name="email")
	private String email;

	// mappedBy must be equal to the name of the attribute in LifeStatus that maps this relation
	@OneToMany(mappedBy="person",cascade=CascadeType.ALL,fetch=FetchType.EAGER)
	@XmlTransient private List<LifeStatus> lifeStatus;
	@OneToMany(mappedBy="person",cascade=CascadeType.ALL,fetch=FetchType.EAGER)
    private List<HealthMeasureHistory>measureHistory;
	public Person() {
	}
	

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	
	public String getLastname() {
		return this.lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public List<LifeStatus> getLifeStatus() {
		/**
		 * if lifestaus is not initialized,make one
		 */
		if(lifeStatus==null)
		{
			lifeStatus=new ArrayList<>();
		}
	    return this.lifeStatus;
	}

	public List<HealthMeasureHistory> getMeasureHistory() {
		/**
		 * if not initialized,create one
		 */
		if(measureHistory==null)
		{
			measureHistory=new ArrayList<>();
		}
		return this.measureHistory;
	}

	public void setMeasureHistory(List<HealthMeasureHistory> measureHistory) {
		this.measureHistory = measureHistory;
	}

	public void setLifeStatus(List<LifeStatus> param) {
	    this.lifeStatus = param;
	}
	
	public LifeStatus addLifeStatus(LifeStatus lifeStatus) {
		lifeStatus.setPerson(this);
		getLifeStatus().add(lifeStatus);
		return lifeStatus;
	}
	public LifeStatus removeLifeStatus(LifeStatus lifeStatus)
	{
		getLifeStatus().remove(lifeStatus);
		lifeStatus.setPerson(this);
		return lifeStatus;
	}
	public HealthMeasureHistory addHealthMeasureHistory(HealthMeasureHistory measureHistory)
	{
		measureHistory.setPerson(this);
		getMeasureHistory().add(measureHistory);
		return measureHistory;
	}
	public HealthMeasureHistory removeHealthMeasureHistory(HealthMeasureHistory measureHistory)
	{
		getMeasureHistory().remove(measureHistory);
		measureHistory.setPerson(this);
		return measureHistory;
	}
	
	public static Person getPersonById(int personId) {
		EntityManager em = LifeCoachDao.instance.getEntityManager();
		Person p = em.find(Person.class, personId);
		return p;
	}
	
	public static List<Person> getAll() {

		System.out.println("--> Initializing Entity manager...");
		EntityManager em = LifeCoachDao.instance.getEntityManager();
		System.out.println("--> Querying the database for all the people...");
	    List<Person> list = em.createNamedQuery("Person.findAll", Person.class).getResultList();
		System.out.println("--> Closing connections of entity manager...");
	    return list;
	}
	
	public static Person savePerson(Person p) {
		EntityManager em = LifeCoachDao.instance.getEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		em.persist(p);
		tx.commit();
	    return p;
	}
	
	public static Person updatePerson(Person p) {
		EntityManager em = LifeCoachDao.instance.getEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		p=em.merge(p);
		tx.commit();
	    return p;
	}
	
	public static void removePerson(Person p) {
		EntityManager em = LifeCoachDao.instance.getEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();
	    p=em.merge(p);
	    em.remove(p);
	    tx.commit();
	}
	@Override
	public String toString()
	{
		return email;
		
	}
}