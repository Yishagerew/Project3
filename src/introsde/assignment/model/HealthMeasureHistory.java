package introsde.assignment.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import introsde.assignment.dao.LifeCoachDao;

@Entity
@Table(name="HealthMeasureHistory")

@NamedQueries({
			@NamedQuery(name="HealthMeasureHistory.findAll", query="SELECT h FROM HealthMeasureHistory h"),
			@NamedQuery(name="HealthMeasureHistory.findByPidMeasureName",query="SELECT h FROM HealthMeasureHistory h WHERE h.person = :person and h.measureDefinition=:measureDefinition"),
			@NamedQuery(name="HealthMeasureHistory.findByPidMeasureNMid",query="SELECT h FROM HealthMeasureHistory h WHERE h.person = :person and h.measureDefinition=:measureDefinition and h.mid=:mid"),
			@NamedQuery(name="HealthMeasureHistory.findByDateRange",query="SELECT h FROM HealthMeasureHistory h WHERE h.person=:person and h.measureDefinition=:measureDefinition and h.dateRegistered BETWEEN :start and :end")
})
@XmlAccessorType(XmlAccessType.FIELD)
public class HealthMeasureHistory implements Serializable{
	private static final long serialVersionUID = 1899889L;

	@TableGenerator(name="sqlite_mhistory", table="sqlite_sequence",
		    pkColumnName="name", valueColumnName="seq",
		    pkColumnValue="HealthMeasureHistory")
	@Id
	@GeneratedValue(strategy=GenerationType.TABLE,generator="sqlite_mhistory")
	
	@Column(name="mid")
	private int mid;

	@Column(name="dateRegistered")
	private String dateRegistered;

	@Column(name="measureValue")
	private String measureValue;

	@OneToOne
	@JoinColumn(name = "measureType", referencedColumnName = "idMeasureDef",insertable=true,updatable=true)
	private MeasureDefinition measureDefinition;

	// notice that we haven't included a reference to the history in Person
	// this means that we don't have to make this attribute XmlTransient
	@ManyToOne
	@JoinColumn(name = "personId", referencedColumnName = "id")
	@XmlTransient private Person person;

	public HealthMeasureHistory() {
	}

	

	public int getMid() {
		return mid;
	}



	public void setMid(int mid) {
		this.mid = mid;
	}



	public String getDateRegistered() {
		return dateRegistered;
	}



	public void setDateRegistered(String dateRegistered) {
		this.dateRegistered = dateRegistered;
	}



	public String getMeasureValue() {
		return measureValue;
	}



	public void setMeasureValue(String measureValue) {
		this.measureValue = measureValue;
	}



	public MeasureDefinition getMeasureDefinition() {
	    return measureDefinition;
	}

	public void setMeasureDefinition(MeasureDefinition param) {
	    this.measureDefinition = param;
	}

	public Person getPerson() {
	    return person;
	}

	public void setPerson(Person param) {
	    this.person = param;
	}

	// database operations
	public static HealthMeasureHistory getHealthMeasureHistoryById(int id) {
		EntityManager em = LifeCoachDao.instance.getEntityManager();
		HealthMeasureHistory p = em.find(HealthMeasureHistory.class, id);
		//LifeCoachDao.instance.closeConnections(em);
		return p;
	}
	
	public static List<HealthMeasureHistory> getAll() {
		EntityManager em = LifeCoachDao.instance.getEntityManager();
	    List<HealthMeasureHistory> list = em.createNamedQuery("HealthMeasureHistory.findAll", HealthMeasureHistory.class).getResultList();
	    //LifeCoachDao.instance.closeConnections(em);
	    return list;
	}
	
	public static HealthMeasureHistory saveHealthMeasureHistory(HealthMeasureHistory p) {
		EntityManager em = LifeCoachDao.instance.getEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		em.persist(p);
		tx.commit();
	   // LifeCoachDao.instance.closeConnections(em);
	    return p;
	}
	
	public static HealthMeasureHistory updateHealthMeasureHistory(HealthMeasureHistory p) {
		EntityManager em = LifeCoachDao.instance.getEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		p=em.merge(p);
		tx.commit();
	   // LifeCoachDao.instance.closeConnections(em);
	    return p;
	}
	
	public static void removeHealthMeasureHistory(HealthMeasureHistory p) {
		EntityManager em = LifeCoachDao.instance.getEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();
	    p=em.merge(p);
	    em.remove(p);
	    tx.commit();
	   // LifeCoachDao.instance.closeConnections(em);
	}
}
