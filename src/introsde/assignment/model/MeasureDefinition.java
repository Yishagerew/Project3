package introsde.assignment.model;
import java.io.Serializable;
import java.util.List;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import introsde.assignment.dao.LifeCoachDao;
/**
* The persistent class for the "MeasureDefinition" database table.
*
*/
@Entity
@Table(name="MeasureDefinition")
@NamedQueries({
	           @NamedQuery(name="measuredefinition.findAll",query="SELECT m FROM MeasureDefinition m "),
	           @NamedQuery(name="measuredefinition.findById",query="SELECT m FROM MeasureDefinition m WHERE m.idMeasureDef=:idmeasuredef"),
	           @NamedQuery(name="measuredefinition.findBymeasureName",query="SELECT m FROM MeasureDefinition m WHERE m.measureName=:measureName")
})
@XmlRootElement(name="measures")
public class MeasureDefinition implements Serializable {
private static final long serialVersionUID = 1L;

@TableGenerator(name="sqlite_measuredef", table="sqlite_sequence",
pkColumnName="name", valueColumnName="seq",
pkColumnValue="MeasureDefinition")
@Id
@GeneratedValue(strategy=GenerationType.TABLE,generator="sqlite_measuredef")

@Column(name="idMeasureDef")
private int idMeasureDef;
@Column(name="measureName")
private String measureName;
@Column(name="measureValueType")
private String measureValueType;
public MeasureDefinition() {
}
public int getIdMeasureDef() {
return this.idMeasureDef;
}
public void setIdMeasureDef(int idMeasureDef) {
this.idMeasureDef = idMeasureDef;
}
public String getMeasureName() {
return this.measureName;
}
public void setMeasureName(String measureName) {
this.measureName = measureName;
}

public String getMeasureValueType() {
	return measureValueType;
}
public void setMeasureValueType(String measureValueType) {
	this.measureValueType = measureValueType;
}
// database operations
public static MeasureDefinition getMeasureDefinitionById(int personId) {
EntityManager em = LifeCoachDao.instance.getEntityManager();
MeasureDefinition p = em.find(MeasureDefinition.class, personId);
return p;
}
public static List<MeasureDefinition> getAll() {
EntityManager em = LifeCoachDao.instance.getEntityManager();
List<MeasureDefinition> list = em.createNamedQuery("MeasureDefinition.findAll", MeasureDefinition.class).getResultList();
//LifeCoachDao.instance.closeConnections(em);
return list;
}
public static MeasureDefinition saveMeasureDefinition(MeasureDefinition p) {
EntityManager em = LifeCoachDao.instance.getEntityManager();
EntityTransaction tx = em.getTransaction();
tx.begin();
em.persist(p);
tx.commit();
//LifeCoachDao.instance.closeConnections(em);
return p;
}
public static MeasureDefinition updateMeasureDefinition(MeasureDefinition p) {
EntityManager em = LifeCoachDao.instance.getEntityManager();
EntityTransaction tx = em.getTransaction();
tx.begin();
p=em.merge(p);
tx.commit();
//LifeCoachDao.instance.closeConnections(em);
return p;
}
public static void removeMeasureDefinition(MeasureDefinition p) {
EntityManager em = LifeCoachDao.instance.getEntityManager();
EntityTransaction tx = em.getTransaction();
tx.begin();
p=em.merge(p);
em.remove(p);
tx.commit();
//LifeCoachDao.instance.closeConnections(em);
}
}