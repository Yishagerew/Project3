package introsde.assignment.delegate;

import introsde.assignment.model.LifeStatus;
import introsde.assignment.model.MeasureDefinition;
import introsde.assignment.transferObject.TLifeStatus;
import introsde.assignment.transferObject.TMeasureDefinition;

import java.util.ArrayList;
import java.util.List;

import org.dozer.DozerBeanMapper;

public class MeasureDefinitionDelegate {
	public List<TMeasureDefinition> mapFromPerson(List<MeasureDefinition> sourceObject) {
		/***************************************************************
		 * myMappingFiles holds the mapping attributes as an arraylist *
		 * which is used for mapping                                   *
		 ***************************************************************/
		List<String> myMappingFiles = new ArrayList<String>();
		/** Mapping xml file containing source and transfer object mapping*/
		myMappingFiles.add("DozerMappingMdefinitions.xml"); 
		DozerBeanMapper mapper = new DozerBeanMapper();
		mapper.setMappingFiles(myMappingFiles);
		List<TMeasureDefinition> mappedDefinitions = new ArrayList<>();
		/************************************************************
		 * Map over **each object** of measure definition to its transfer object*
		 * On a row basis                                           *
		 ************************************************************/
		for (MeasureDefinition m : sourceObject) {
			mappedDefinitions.add(mapper.map(m, TMeasureDefinition.class));
				}
		return mappedDefinitions;
	}
}
