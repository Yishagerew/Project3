package introsde.assignment.delegate;

import java.util.ArrayList;
import java.util.List;

import org.dozer.DozerBeanMapper;

import introsde.assignment.model.LifeStatus;
import introsde.assignment.transferObject.TLifeStatus;

public class LifeStatusDelegate {
	public List<TLifeStatus> mapFromPerson(List<LifeStatus> sourceObject) {
		/***************************************************************
		 * myMappingFiles holds the mapping attributes as an arraylist *
		 * which is used for mapping                                   *
		 ***************************************************************/
		List<String> myMappingFiles = new ArrayList<String>();
		/** Mapping xml file containing source and transfer object mapping*/
		myMappingFiles.add("DozerMapping.xml"); 
		DozerBeanMapper mapper = new DozerBeanMapper();
		mapper.setMappingFiles(myMappingFiles);
		List<TLifeStatus> mappedPeoples = new ArrayList<TLifeStatus>();
		/************************************************************
		 * Map over **each object** of person to its transfer object*
		 * On a row basis                                           *
		 ************************************************************/
		for (LifeStatus l : sourceObject) {
				mappedPeoples.add(mapper.map(l, TLifeStatus.class));
				}
		return mappedPeoples;
	}
}
