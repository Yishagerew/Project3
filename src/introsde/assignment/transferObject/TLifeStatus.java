package introsde.assignment.transferObject;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import introsde.assignment.model.Person;
@XmlRootElement(name="Person")
@XmlAccessorType(XmlAccessType.FIELD)
public class TLifeStatus {
private Person person;

public Person getPerson() {
	return person;
}

public void setPerson(Person person) {
	this.person = person;
}

}
