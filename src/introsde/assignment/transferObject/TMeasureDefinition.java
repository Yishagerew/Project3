package introsde.assignment.transferObject;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="measureTypes")
@XmlAccessorType(XmlAccessType.FIELD)
public class TMeasureDefinition {
	private String measureName;

	public String getMeasureName() {
		return measureName;
	}

	public void setMeasureName(String measureName) {
		this.measureName = measureName;
	}
	

}
