package introsde.assignment.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import introsde.assignment.dao.LifeCoachDao;
import introsde.assignment.delegate.MeasureDefinitionDelegate;
import introsde.assignment.model.HealthMeasureHistory;
import introsde.assignment.model.LifeStatus;
import introsde.assignment.model.MeasureDefinition;
import introsde.assignment.model.Person;
import introsde.assignment.transferObject.TMeasureDefinition;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Query;


public class PeopleLogicImpl {
public static int addNewPerson(Person person)
{
person.setLifeStatus(null);
person.setMeasureHistory(null);
System.out.println("Measures set to null");
EntityManager em=LifeCoachDao.instance.getEntityManager();	
EntityTransaction tx=em.getTransaction();
tx.begin();
em.persist(person);
tx.commit();
return person.getId();
}
@SuppressWarnings("unchecked")
public static List<Person>readPersonList()
{
	System.out.println("Atleast entered");
	EntityManager em=LifeCoachDao.instance.getEntityManager();
	Query query=em.createNamedQuery("Person.findAll",Person.class);
	List<Person>personList;
	try
	{
		personList=query.getResultList();
		System.out.println("entered and retrieved"+personList.get(0).getFirstname());
		return personList;
	}
	catch(Exception e)
	{
		System.out.println(e.getMessage());
		return null;
	}
}
/**
 * A method 
 * @param id
 * @return
 */
public static Person readPerson(int id)
{
Person person=findPersonById(id);
return person;
}
/**
 * Updating a person 
 * @param person
 *        A person Object
 * @return
 */
public static Person updatePerson(Person person)
{
	/**
	 * only person information is updated
	 */
	person.setLifeStatus(null);
	person.setMeasureHistory(null);
	EntityManager em=LifeCoachDao.instance.getEntityManager();
	EntityTransaction tx=em.getTransaction();
	tx.begin();
	try
	{
	person=em.merge(person);
	}
	catch(Exception e)
	{
		System.out.println(e.getMessage());
	}
	tx.commit();
	return person;
	
}
/**
 * A method for creating a new person and persist to the database
 * @param person
 * @return
 */
public static Person createPerson(Person person)
{
	/**
	 * needs a check on inputs
	 */
	EntityManager em=LifeCoachDao.instance.getEntityManager();
	EntityTransaction tx=em.getTransaction();
	tx.begin();
	em.persist(person);
	tx.commit();
	return person;
}
/**
 * A method for deleting a person given its Id
 * @param personId
 *        A person Id
 * @return
 */
public static void deletePerson(int personId)
{
	EntityManager em=LifeCoachDao.instance.getEntityManager();
	Person person=findPersonById(personId);
	EntityTransaction tx=em.getTransaction();
	tx.begin();
	em.remove(person);
	tx.commit();
	
}
@SuppressWarnings("unchecked")
public static List<HealthMeasureHistory> readPersonHistory(int id,String measureType)
{
	EntityManager em=LifeCoachDao.instance.getEntityManager();
	Query historyQuery=em.createNamedQuery("HealthMeasureHistory.findByPidMeasureName",HealthMeasureHistory.class);
	historyQuery.setParameter("person",findPersonById(id));
	historyQuery.setParameter("measureDefinition", getMeasureDefinition(measureType));
	List<HealthMeasureHistory>measureHistories=null;
	try
	{
		measureHistories=historyQuery.getResultList();
	}
	catch(Exception e)
	{
		System.out.println(e.getMessage());
	}
	return measureHistories;	
}
public static HealthMeasureHistory readPersonMeasurement(int id,String measureType,int mid)
{
	EntityManager em=LifeCoachDao.instance.getEntityManager();
	Query definitionQuery=em.createNamedQuery("HealthMeasureHistory.findByPidMeasureNMid",HealthMeasureHistory.class);
	definitionQuery.setParameter("person",findPersonById(id));
	definitionQuery.setParameter("measureDefinition", getMeasureDefinition(measureType));
	definitionQuery.setParameter("mid", mid);
	HealthMeasureHistory measureHistory=null;
	try
	{
		measureHistory=(HealthMeasureHistory) definitionQuery.getSingleResult();
	}
	catch(NoResultException e)
	{
		System.out.println(e.getMessage());
	}
	return measureHistory;
  	
}
public static void savePersonMeasurement(int id,HealthMeasureHistory m)
{
	MeasureDefinition mDef=m.getMeasureDefinition();
	Person person=findPersonById(id);
	/**
	 * retrieve a life status record matching a measure definition
	 */
	EntityManager em=LifeCoachDao.instance.getEntityManager();
	Query statusQuery=em.createNamedQuery("LifeStatus.findByMeasureDef",LifeStatus.class);
	statusQuery.setParameter("measureDefinition", mDef);
	statusQuery.setParameter("person", person);
	LifeStatus lifeStatus=null;
	try
	{
		lifeStatus=(LifeStatus) statusQuery.getSingleResult();
	}
	catch(NoResultException e)
	{
		System.out.println(e.getMessage());
	}
	/**
	 * check if life status info found
	 */
	if(lifeStatus==null)
	{
		/**
		 * add to life status only
		 */
		lifeStatus=new LifeStatus();
		lifeStatus.setMeasureDefinition(mDef);
		lifeStatus.setPerson(person);
		lifeStatus.setMeasureValue(Integer.parseInt(m.getMeasureValue()));
		addNewLifeStatus(lifeStatus);	
	}
	/**
	 * If there is an old value stored for that measure name,archive old value to history and update lifestatus information
	 */
	else
	{
	HealthMeasureHistory newPersonHistory=new HealthMeasureHistory();
	newPersonHistory.setMeasureDefinition(lifeStatus.getMeasureDefinition());
	newPersonHistory.setMeasureValue(String.valueOf(lifeStatus.getMeasureValue()));
	newPersonHistory.setPerson(lifeStatus.getPerson());
	addNewHealthMeasureHistory(newPersonHistory);
	/**
	 * update file on Lifestatus of the existing value
	 */
	lifeStatus.setMeasureValue(Integer.parseInt(m.getMeasureValue()));
	updateLifeStatus(lifeStatus);
	}
	
}
public static List<MeasureDefinition> readMeasureTypes()
{
	EntityManager em=LifeCoachDao.instance.getEntityManager();
	Query measureQuery=em.createNamedQuery("measuredefinition.findAll",MeasureDefinition.class);
	@SuppressWarnings("unchecked")
	List<MeasureDefinition> mDefinitions=measureQuery.getResultList();
	//MeasureDefinitionDelegate mDelegate=new MeasureDefinitionDelegate();
	
	//return mDelegate.mapFromPerson(mDefinitions);
	return mDefinitions;
}
public static void updatePersonMeasure(int id,HealthMeasureHistory m)
{
	Person person=findPersonById(id);
	m.setPerson(person);
	m.setMid(m.getMid());
	updateHealthMeasureHistory(m);
}
public static List<HealthMeasureHistory>readPersonMeasureByDates(int id, String measureType, Date before, Date after)
{
	EntityManager em=LifeCoachDao.instance.getEntityManager();
	Query personQuery=em.createNamedQuery("HealthMeasureHistory.findByDateRange",HealthMeasureHistory.class);
	personQuery.setParameter("person", findPersonById(id));
	personQuery.setParameter("measureDefinition", getMeasureDefinition(measureType));
	personQuery.setParameter("min", before);
	personQuery.setParameter("max", after);
	@SuppressWarnings("unchecked")
	List<HealthMeasureHistory>healthMeasures=personQuery.getResultList();
	return healthMeasures;
}
public static List<Person> readPersonListByMeasurement(String measureType, String ...values)
{
	/**
	 * *******************************************************************
	 * ***IMPORTANT:******************************************************
	 * For this to work,the client should send 2 parameters***************
	 * ***but can both be empty or the given value is for min value values given***************************
	 * *******************************************************************/
	List<Person>peoples=new ArrayList<Person>();
	EntityManager em=LifeCoachDao.instance.getEntityManager();
	Query historyQuery=null;
	/**
	 * If all params are given
	 */
	if(values==null)
	{
		historyQuery=em.createNamedQuery("LifeStatus.findByMeasureName",LifeStatus.class);
		historyQuery.setParameter("measureDefinition", getMeasureDefinition(measureType));
	}
	else if(values.length==2)
	{
		String minValue=(Integer.parseInt(values[0]) > Integer.parseInt(values[1]))?values[1]:values[0];
		String maxValue=(Integer.parseInt(values[0]) > Integer.parseInt(values[1]))?values[0]:values[1];
		System.out.println("Minimum Value"+minValue);
		System.out.println("Maximum Value"+maxValue);
		historyQuery=em.createNamedQuery("LifeStatus.findByMinMaxRange",LifeStatus.class);
	historyQuery.setParameter("measureDefinition", getMeasureDefinition(measureType));
	historyQuery.setParameter("min", Integer.parseInt(minValue));
	historyQuery.setParameter("max", Integer.parseInt(maxValue));
	}
	/**
	 * The the minimum is give and taken by assumption
	 */
	else
	{
		historyQuery=em.createNamedQuery("LifeStatus.findByMinRange",LifeStatus.class);
		historyQuery.setParameter("measureDefinition", getMeasureDefinition(measureType));
		historyQuery.setParameter("min", values[0]);	
	}
	@SuppressWarnings("unchecked")
	List<LifeStatus>lifeStatuses=historyQuery.getResultList();
	/**
	 * get all person objects from measurehistory
	 */
	for(LifeStatus statuses:lifeStatuses)
	{
		peoples.add(statuses.getPerson());
	}
	return peoples;	
}




/**
 * Helper method for finding a person by its Id
 * @param personId
 *        A person id
 * @return
 */
public static Person findPersonById(int personId)
{
	EntityManager em=LifeCoachDao.instance.getEntityManager();
	Query personQuery=em.createNamedQuery("person.findById",Person.class);
	personQuery.setParameter("personId", personId);
	Person person=null;
	try
	{
		person=(Person) personQuery.getSingleResult();

	}
	catch(NoResultException e)
	{
		System.out.println(e.getMessage());
	}
	return person;
}
public static MeasureDefinition getMeasureDefinition(String measureType)
{
	EntityManager em=LifeCoachDao.instance.getEntityManager();
	Query definitionQuery=em.createNamedQuery("measuredefinition.findBymeasureName",MeasureDefinition.class);
	definitionQuery.setParameter("measureName", measureType);
	MeasureDefinition mdef=null;
	try
	{
	mdef=(MeasureDefinition) definitionQuery.getSingleResult();	
	}
	catch(NoResultException e)
	{
		System.out.println(e.getMessage());
	}
	return mdef;
}
public static void  addNewLifeStatus(LifeStatus lifeStatus)
{
EntityManager em=LifeCoachDao.instance.getEntityManager();
EntityTransaction tx=em.getTransaction();
tx.begin();
em.persist(lifeStatus);
tx.commit();
}
public static void addNewHealthMeasureHistory(HealthMeasureHistory mhistory)
{
	EntityManager em=LifeCoachDao.instance.getEntityManager();
	EntityTransaction tx=em.getTransaction();
	tx.begin();
	em.persist(mhistory);
	tx.commit();	
}
public static void updateLifeStatus(LifeStatus lifeStatus)
{
		EntityManager em=LifeCoachDao.instance.getEntityManager();
		EntityTransaction tx=em.getTransaction();
		tx.begin();
		em.merge(lifeStatus);
		tx.commit();	
}
/**
 * Create a person list for setting */
public static void updateHealthMeasureHistory(HealthMeasureHistory mHistory)
{
	EntityManager em=LifeCoachDao.instance.getEntityManager();
	EntityTransaction tx=em.getTransaction();
	tx.begin();
	em.merge(mHistory);
	tx.commit();	
}
}
