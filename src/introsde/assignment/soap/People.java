package introsde.assignment.soap;

import introsde.assignment.model.HealthMeasureHistory;
import introsde.assignment.model.MeasureDefinition;
import introsde.assignment.model.Person;
import introsde.assignment.transferObject.TMeasureDefinition;

import java.util.Date;
import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;
import javax.jws.soap.SOAPBinding.Use;
import javax.xml.bind.annotation.XmlElementWrapper;

@WebService
@SOAPBinding(style = Style.DOCUMENT, use=Use.ENCODED) 
public interface People {
	/**
	 * A method for reading the list of all peoples
	 * @return
	 */
	@WebMethod(operationName="readPersonList")
	@WebResult(name="people")
	public List<Person> readPersonList();
	/**
	 * A method for returning a person given Id
	 * @param id
	 *        A person Id
	 * @return
	 *       Returns a person object
	 */
	@WebMethod(operationName="readPerson")
	@WebResult(name="person")
	public Person readPerson(@WebParam(name="personId") int id);
	/**
	 *A method for updating the detail of a person 
	 * @param id
	 *        A person Id
	 * @return
	 *        Return the Id of the updated person Info
	 */
	@WebMethod(operationName="updatePerson")
	@WebResult(name="person")
	public Person updatePerson(@WebParam(name="personId") Person person);
	
	/**
	 * A method for a
	 * dding new person with profile information if given
	 * @param person
	 * @return
	 */
	@WebMethod(operationName="createPerson")
	@WebResult(name="personId")
	public int createPerson(@WebParam(name="person") Person person);
	/**
	 * A method for deleting a person given its id
	 * @param id
	 *        A person Id
	 * @return
	 *       Return the id of the deleted person
	 */
	@WebMethod(operationName="deletePerson")
	@WebResult(name="personId")
	public int deletePerson(@WebParam(name="personId") int id);
	/**
	 * A method for returning the history of a person given person Id and Measure name for the history
	 * @param id
	 *        A person id
	 * @param measureType
	 *       A measure name ex.Height
	 * @return
	 */
	@WebMethod(operationName="readPersonHistory")
	@WebResult(name="healthProfile-history")
	public List<HealthMeasureHistory> readPersonHistory(@WebParam(name="personId") int id,@WebParam(name = "measureType", partName = "measureType") String measureType);
	/**
	 * A method for returning the value of a measure identified by person Id and measure Id[mid]
	 * of a measure name [measureType]
	 * @param id
	 *        A person Id
	 * @param measureType
	 *        A measure Name
	 * @param mid
	 *        A measure Id
	 * @return
	 *       Returns the value of the measure
	 */
	@WebMethod(operationName="readPersonMeasurement")
	@WebResult(name="value")
	public HealthMeasureHistory readPersonMeasurement(@WebParam(name="personId") int id,
			                                          @WebParam(name="measureType") String measureType,
			                                          @WebParam(name="mid") int mid);
	/**
	 * A method for archiving old value of current measure profile and updating with new measured value
	 * Also archives the old value to personal measure histories
	 * @param id
	 * @param measureType
	 * @return
	 */
	@WebMethod(operationName="savePersonMeasurement")
	@WebResult(name="personId")
	public int savePersonMeasurement(@WebParam(name="personId") int id,@WebParam(name="measureHistory") HealthMeasureHistory measureHistory);
	
	/**
	 * A method for returning the set of measure types/measure names Ex.Weight,Height...
	 * @return
	 */
	@WebMethod(operationName="readMeasureTypes")
	@WebResult(name="measureTypes")
	public List<MeasureDefinition> readMeasureTypes();
	
	/**
	 * A method for updating the measure of a person identified by id,measure name by MeasureType
	 * and its Mid
	 * @param id
	 *        A person Id
	 * @param measureType
	 *        A measure Type
	 * @param mid
	 *        A measure Id
	 * @return
	 *        Returns the updated id of a person
	 */
	@WebMethod(operationName="updatePersonMeasure")
	@WebResult(name="personId")
	public int updatePersonMeasure(@WebParam(name="personId") int id,
			                       @WebParam(name="measureHistory") HealthMeasureHistory measureHistory);
	
	/**
	 * A method for returning lsit of measure histories
	 * @param id
	 *         A person Id
	 * @param measureType
	 *         A measure Name ex.Height
	 * @param before
	 *         A date used for setting starting date
	 * @param after
	 *        A date used for setting ending date
	 * @return
	 *        Returns list of meaasure histories
	 */
	@WebMethod(operationName="readPersonMeasureByDates")
	@WebResult(name="healthProfile-history")
	public List<HealthMeasureHistory>readPersonMeasureByDates(@WebParam(name="personId") int id,
			                                                  @WebParam(name="measureType") String measureType,
			                                                  @WebParam(name="before") Date before,
			                                                  @WebParam(name="after") Date after);
			                                                     
	/**
	 * A method for returning the list of people with the min max range identified by measure name                                                           
	 * @param measureType
	 *        Measure name
	 * @param min
	 *        Min value of the measure
	 * @param max
	 *        Max value of the measure
	 * @return
	 *        The list of people satisfying the criteria
	 */
	@WebMethod(operationName="readPersonListByMeasurement")
	@WebResult(name="people")
	public List<Person>readPersonListByMeasurement(@WebParam(name="measureType") String measureType,
			                                       @WebParam(name="min") String min,
			                                       @WebParam(name="max") String max);
}
