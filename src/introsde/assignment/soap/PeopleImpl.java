package introsde.assignment.soap;

import introsde.assignment.business.PeopleLogicImpl;
import introsde.assignment.model.HealthMeasureHistory;
import introsde.assignment.model.MeasureDefinition;
import introsde.assignment.model.Person;
import introsde.assignment.transferObject.TMeasureDefinition;

import java.util.Date;
import java.util.List;

import javax.jws.WebService;
@WebService(endpointInterface = "introsde.assignment.soap.People",
serviceName="PeopleService")
public class PeopleImpl implements People {

	@Override
	public List<Person> readPersonList() {
		return PeopleLogicImpl.readPersonList();
	}

	@Override
	public Person readPerson(int id) {
		System.out.println("From the server"+PeopleLogicImpl.readPerson(id));
		return PeopleLogicImpl.readPerson(id);
	}

	@Override
	public int deletePerson(int id) {
		PeopleLogicImpl.deletePerson(id);
		return id; 
	}

	@Override
	public List<HealthMeasureHistory> readPersonHistory(int id, String measureType) {
		return PeopleLogicImpl.readPersonHistory(id, measureType);
	}

	@Override
	public HealthMeasureHistory readPersonMeasurement(int id,
			String measureType, int mid) {
		return PeopleLogicImpl.readPersonMeasurement(id, measureType, mid);
	}

	@Override
	public int savePersonMeasurement(int id, HealthMeasureHistory measureHistory) {
		PeopleLogicImpl.savePersonMeasurement(id, measureHistory);
		return id;
	}

	@Override
	public List<MeasureDefinition> readMeasureTypes() {
		return PeopleLogicImpl.readMeasureTypes();
	}

	@Override
	public int updatePersonMeasure(int id, HealthMeasureHistory measureHistory) {
		PeopleLogicImpl.updatePersonMeasure(id, measureHistory);
		return 1;
	}

	@Override
	public List<HealthMeasureHistory> readPersonMeasureByDates(int id,
			String measureType, Date before, Date after) {
		return PeopleLogicImpl.readPersonMeasureByDates(id, measureType, before, after);
	}

	@Override
	public List<Person> readPersonListByMeasurement(String measureType,
			String min, String max) {
      return PeopleLogicImpl.readPersonListByMeasurement(measureType, min, max);
	}

	@Override
	public Person updatePerson(Person person) {
		return PeopleLogicImpl.updatePerson(person);
	}

	@Override
	public int createPerson(Person person) {
		System.out.println("Atleast called for creations");
		System.out.println(person);
		System.out.println("Nothing happened");
//System.out.println("Can retrieve form the given object"+person.getFirstname());
		return PeopleLogicImpl.addNewPerson(person);
	}

	

	

}
