package introsde.assignment.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import introsde.assignment.business.PeopleLogicImpl;
import introsde.assignment.dao.LifeCoachDao;
import introsde.assignment.model.LifeStatus;
import introsde.assignment.model.MeasureDefinition;
import introsde.assignment.model.Person;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.Metamodel;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

public class PersonTest {
@Ignore
	@Test
	public void readPersonListTest() {
	System.out.println("--> TEST: readPersonList");
	System.out.println("But we are here---------------------------------------------");
	List<Person> list = em.createNamedQuery("Person.findAll",Person.class)
	.getResultList();
	System.out.println("we have this number of people="+list.size());
	for (Person person : list) {
	System.out.println("--> Person = "+"Name"+ person.getFirstname() +":"+person.getLastname());
	}
	assertTrue(list.size()>0);
	}

@Ignore
	@Test
	public void addPersonWithStatus()
	{
		Person person=new Person();
		person.setFirstname("kedir");
		
		EntityManager em=LifeCoachDao.instance.getEntityManager();
		MeasureDefinition mdef=new MeasureDefinition();
		Query query=em.createNamedQuery("measuredefinition.findById",MeasureDefinition.class);
		query.setParameter("idmeasuredef", 1);
		LifeStatus status1=new LifeStatus();
		try
		{
		mdef=(MeasureDefinition) query.getSingleResult();
		System.out.println("Measuredefinition retrieved"+mdef.getMeasureName());
		}
		catch(NoResultException e)
		{
		System.out.println(e.getMessage());	
		}
		status1.setMeasureValue(400);
		status1.setMeasureDefinition(mdef);
		person.addLifeStatus(status1);
        
//		hprofile=person.getLifeStatus();
//		if(hprofile.size()>0)
//		{
//			person.setLifeStatus(hprofile);	
//		}
		EntityTransaction tx=em.getTransaction();
		tx.begin();
		em.persist(person);
		tx.commit();
		
	}
	@Ignore
	@Test
		public void getPersonDetail() {
		System.out.println("--->Reading a single person");
			//EntityManager em = LifeCoachDao.instance.getEntityManager();
			Person person = PeopleLogicImpl.findPersonById(1);
			//Query query = em.createNamedQuery("person.findById").setParameter(
			//		"idPerson", 1);
			//person = (Person) query.getSingleResult();
			if(person==null)
			{
				throw new RuntimeException("Person with this id not found");
			}
			System.out.println(person.getLastname());
		}
		
	
	// test for adding person without using the DAO object, but isntead using the entity manager
	// created in the testing unit by the beforetest method
	@Ignore
	@Test
	public void addPersonTest() {
	System.out.println("--> TEST: addPerson");
	// count people before starting
	List<Person> list = em.createNamedQuery("Person.findAll", Person.class)
	.getResultList();
	int originalCount = list.size();
	// Arrange
	Person at = new Person();
	at.setFirstname("feu");
	at.setLastname("Lulie");
	Calendar cal = Calendar.getInstance();
	cal.set(1979, Calendar.DECEMBER, 12);
	System.out.println(cal.getTime());
	System.out.println("--> TEST: addPerson => persisting the new person");
	// Act
	tx.begin();
	em.persist(at);
	tx.commit();
	// Assert
	assertNotNull("Id should not be null", at.getId());
	// Query using CriteriaBuilder and CriteriaQuery Example
	int idNewPerson = at.getId();
	System.out.println("--> TEST: addPerson => querying for the new person");
	// 1. Prepare the criteria builder and the criteria query to work with the model you want
	CriteriaBuilder cb = em.getCriteriaBuilder();
	CriteriaQuery<Person> cq = cb.createQuery(Person.class);
	// setup a metamodel for the root of the query
	Metamodel m = em.getMetamodel();
	EntityType<Person> Person_ = m.entity(Person.class);
	Root<Person> person = cq.from(Person_);
	// prepare a parameter expression to setup parameters for the query
	ParameterExpression<Integer> p = cb.parameter(Integer.class);
	// prepare the query
	cq.select(person).where(cb.equal(person.get("idPerson"), p));
	// execute the query and set the parameters with values
	TypedQuery<Person> tq = em.createQuery(cq);
	tq.setParameter(p, idNewPerson);
	// get the results
	List<Person> newPersonList = tq.getResultList();
	assertEquals("Table has one entities", 1, newPersonList.size());
	assertEquals("Table has correct name", "Pinci", newPersonList.get(0).getFirstname());
	System.out.println("--> TEST: addPerson => deleting the new person");
	em.getTransaction().begin();
	em.remove(newPersonList.get(0));
	em.getTransaction().commit();
	list = em.createNamedQuery("Person.findAll", Person.class)
	.getResultList();
	assertEquals("Table has no entity", originalCount, list.size());
	}
	// same adding person test, but using the DAO object
	@Ignore
	@Test
	public void addPersonWithDaoTest() {
		 System.out.println("--> TEST: addPersonWithDao");

	        List<Person> list = Person.getAll();
	        int personOriginalCount = list.size();

	        Person p = new Person();
	        p.setFirstname("Pinco");
	        p.setLastname("Pallino");
	        Calendar c = Calendar.getInstance();
	        c.set(1984, 6, 21);

	        System.out.println("--> TEST: addPersonWithDao ==> persisting person");
	        Person.savePerson(p);
	        assertNotNull("Id should not be null", p.getId());

	        System.out.println("--> TEST: addPersonWithDao ==> getting the list");
	        list = Person.getAll();
	        assertEquals("Table has two entities", personOriginalCount+1, list.size());

	        Person newPerson = Person.getPersonById(p.getId());

	        System.out.println("--> TEST: addPersonWithDao ==> removing new person");
	        Person.removePerson(newPerson);
	        list = Person.getAll();
	        assertEquals("Table has two entities", personOriginalCount, list.size());
	}
	@SuppressWarnings("unchecked")
	@Ignore
	@Test
	public void getAllPersons()
	{
		EntityManager em=LifeCoachDao.instance.getEntityManager();
		Query query=em.createNamedQuery("Person.findAll",Person.class);
		List<Person>personList=new ArrayList<Person>();
		try
		{
			personList=query.getResultList();
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
		for(Person person:personList)
		{
			System.out.println(person.getFirstname());
		}
	}
	
	@Test
	public void addNewPeople()
	{
		Person p=new Person();
		p.setFirstname("Tolosasssssssssssssss");
		PeopleLogicImpl.createPerson(p);
	}
	@BeforeClass
	public static void beforeClass() {
	System.out.println("Testing JPA on lifecoach database using 'introsde-jpa' persistence unit");
	emf = Persistence.createEntityManagerFactory("SoapHealthServices");
	System.out.println("Created EMF is "+emf);
	em = emf.createEntityManager();
	System.out.println("Entity manager created++++++++++++++++++++++++++++++++++"+ em);
	}
	@AfterClass
	public static void afterClass() {
		try
		{
	em.close();
	emf.close();
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
	}
	@Before
	public void before() {
	tx = em.getTransaction();
	}
	private static EntityManagerFactory emf;
	private static EntityManager em;
	private EntityTransaction tx;

}
