package introsde.assignment.test;

import introsde.assignment.model.MeasureDefinition;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class MeasureDefinitionTest {

	@Test
	public void addMeasureDefinition()
	{
		MeasureDefinition mdef=new MeasureDefinition();
		mdef.setIdMeasureDef(1);
		mdef.setMeasureName("weight");
		mdef.setMeasureValueType("double");
		tx.begin();
		em.persist(mdef);
		tx.commit();
	}
	@BeforeClass
	public static void beforeClass() {
	System.out.println("Testing JPA on lifecoach database using 'introsde-jpa' persistence unit");
	emf = Persistence.createEntityManagerFactory("SoapHealthServices");
	System.out.println("Created EMF is "+emf);
	em = emf.createEntityManager();
	System.out.println("Entity manager created++++++++++++++++++++++++++++++++++"+ em);
	}
	@AfterClass
	public static void afterClass() {
		try
		{
	em.close();
	emf.close();
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
	}
	@Before
	public void before() {
	tx = em.getTransaction();
	}
	private static EntityManagerFactory emf;
	private static EntityManager em;
	private EntityTransaction tx;
}
