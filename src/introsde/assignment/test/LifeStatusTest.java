package introsde.assignment.test;
import static org.junit.Assert.*;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import introsde.assignment.business.PeopleLogicImpl;
import introsde.assignment.model.LifeStatus;
import introsde.assignment.model.Person;

public class LifeStatusTest {
/**
    public void readAllLifeStatusListDaoTest() {
        System.out.println("--> TEST: readAllLifeStatusWithDao");
        List<LifeStatus> mList = LifeStatus.getAll();
        for(LifeStatus status:mList)
        {
        	System.out.println(status.getMid());
        	System.out.println(status.getMeasureValue());
        }
        assertTrue("LifeStatus not empty in DB", mList.size()>0);
    } */
    @Test
    public void readLifeStatusInPersonTest() {
        System.out.println("--> TEST: readLifeStatusPersonRelationship");
        // setting weight for an existing person with existing measures
        Person person = PeopleLogicImpl.findPersonById(1);
        assertTrue("Person should have at least one measurement", person.getLifeStatus().size()>0);
        LifeStatus l = person.getLifeStatus().get(1);
        assertNotNull("LifeStatus measure was created", l.getMid());
    }

    @BeforeClass
    public static void beforeClass() {
        System.out.println("Testing JPA on lifecoach database using 'introsde-jpa' persistence unit");
        emf = Persistence.createEntityManagerFactory("SoapHealthServices");
        em = emf.createEntityManager();
    }

    @AfterClass
    public static void afterClass() {
        em.close();
        emf.close();
    }

    @Before
    public void before() {
        tx = em.getTransaction();
    }

    private static EntityManagerFactory emf;
    private static EntityManager em;
    @SuppressWarnings("unused")
	private EntityTransaction tx;
}