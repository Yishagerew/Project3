package introsde.assignment.test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import introsde.assignment.business.PeopleLogicImpl;
import introsde.assignment.model.HealthMeasureHistory;
import introsde.assignment.model.MeasureDefinition;
import introsde.assignment.model.Person;
import introsde.assignment.transferObject.TMeasureDefinition;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class PeopleImplTest {
	/**
	 * Request 1 Test,getting all people list
	 */
	@Ignore
	@Test
	public void getAllPeopleList()
	{
		List<Person>listOfPeople=PeopleLogicImpl.readPersonList();
		for(Person person:listOfPeople)
		{
			System.out.println("Persons:"+person.getFirstname());
		}
		
	}
	
	/**
	 * Request #2,getting a person with his Id
	 */
	@Test
	public void getPersonWithId()
	{
		Person person=PeopleLogicImpl.readPerson(200);
		System.out.println(person.getFirstname());
	}
	/**
	 * Request #3,Update a person information
	 */
	@Ignore
	@Test
	public void updatePerson()
	{
		Person person=new Person();
		person.setId(200);
		person.setFirstname("Lulie");
		PeopleLogicImpl.updatePerson(person);
		String changedName=PeopleLogicImpl.readPerson(200).getFirstname();
		assertEquals("Name should be changed",changedName,"Lulie");
	}
	
	/**
	 * Request #4,create a new person
	 */
	@Ignore
	@Test
	public void createNewPerson()
	{
		int originalCount=PeopleLogicImpl.readPersonList().size();
		Person person=new Person();
		person.setFirstname("Tola");
		PeopleLogicImpl.createPerson(person);
		int newCount=PeopleLogicImpl.readPersonList().size();
		assertEquals("One more person should be found", originalCount+1,newCount);
	}
	
	/**
	 * Request #5,Delete a person
	 */
	@Ignore
	@Test
	public void deletePerson()
	{
		int beforeDeletionCount=PeopleLogicImpl.readPersonList().size();
		Person person=PeopleLogicImpl.readPersonList().get(0);
		PeopleLogicImpl.deletePerson(person.getId());
		int afterDeletionCount=PeopleLogicImpl.readPersonList().size();
		assertEquals("after should be less than before",beforeDeletionCount,afterDeletionCount+1);	
	}
	@Ignore
	@Test
	public void addPersonMeasureHistory()
	{
		/**
		 * get a measure definition object
		 */
		Query definitionQuery=em.createNamedQuery("measuredefinition.findById",MeasureDefinition.class);
		definitionQuery.setParameter("idmeasuredef", 1);
		MeasureDefinition mdef=null;
		try
		{
		 mdef=(MeasureDefinition) definitionQuery.getSingleResult();
		}
		catch(NoResultException e)
		{
			System.out.println(e.getMessage());
		}
		
		HealthMeasureHistory measureHistory=new HealthMeasureHistory();
		measureHistory.setMeasureValue("200");
		measureHistory.setMeasureDefinition(mdef);
		Person person=PeopleLogicImpl.readPersonList().get(0);
        person.addHealthMeasureHistory(measureHistory);
        PeopleLogicImpl.createPerson(person);
	}
	
	/**
	 * Request #6,Read person history
	 */
	@Ignore
	@Test
	public void readPersonMeasurehistory()
	{
		List<HealthMeasureHistory>personMeasures=PeopleLogicImpl.readPersonHistory(2, "weight");
		for(HealthMeasureHistory measures:personMeasures)
		{
			System.out.println(measures.getMeasureValue());
		}
	}
	
	/**
	 * Request #7,get measure history with three parameter
	 */
	@Ignore
	@Test
	public void readPersonMeasureHistoryMoreParams()
	{
		HealthMeasureHistory measure=PeopleLogicImpl.readPersonMeasurement(2, "weight", 11);
		System.out.println(measure.getMeasureValue());
		System.out.println(measure.getMid());
		System.out.println(measure.getDateRegistered());
	}
	
	/**
	 * Request #8,Save person measurement
	 */
	@Ignore
	@Test
	public void savePersonMeasurement()
	{
		Query definitionQuery=em.createNamedQuery("measuredefinition.findById",MeasureDefinition.class);
		definitionQuery.setParameter("idmeasuredef", 1);
		MeasureDefinition mdef=null;
		try
		{
		 mdef=(MeasureDefinition) definitionQuery.getSingleResult();
		}
		catch(NoResultException e)
		{
			System.out.println(e.getMessage());
		}
		HealthMeasureHistory histories=new HealthMeasureHistory();
		histories.setMeasureDefinition(mdef);
		histories.setPerson(PeopleLogicImpl.readPerson(2));
		histories.setMeasureValue("600");
		PeopleLogicImpl.savePersonMeasurement(2, histories);
		/**
		 * manual check,lazy programmer like me do so
		 */
	}
	
	/**
	 * Request #9,Read measure types
	 * implemented with transfer objects
	 */
	@Ignore
	@Test
	public void getMeasureTypes()
	{
		/**
		 * Used for mapping
		 */
		//List<TMeasureDefinition> mdefs=PeopleLogicImpl.readMeasureTypes();
		List<MeasureDefinition> mdefs=PeopleLogicImpl.readMeasureTypes();
		for(MeasureDefinition mdef:mdefs)
		{
			System.out.println(mdef.getMeasureName());
		}
	}
	
	/**
	 * Request #10,update person measure identified with mid
	 */
	@SuppressWarnings("deprecation")
	//@Ignore
	@Ignore
	@Test
	public void updatePersonMeasureWithMid()
	{
		/**
		 * have a definition object
		 */
		Query definitionQuery=em.createNamedQuery("measuredefinition.findById",MeasureDefinition.class);
		definitionQuery.setParameter("idmeasuredef", 1);
		MeasureDefinition mdef=null;
		try
		{
		 mdef=(MeasureDefinition) definitionQuery.getSingleResult();
		}
		catch(NoResultException e)
		{
			System.out.println(e.getMessage());
		}
		HealthMeasureHistory mHistory=new HealthMeasureHistory();
		mHistory.setMeasureDefinition(mdef);
		mHistory.setMid(11);
		mHistory.setMeasureValue("800");
		PeopleLogicImpl.updatePersonMeasure(2, mHistory);
	}
	
	/**
	 * Request 11,retrieve history on date range
	 */
	@Ignore
	@Test
	public void getHistories()
	{
		List<HealthMeasureHistory>histories=PeopleLogicImpl.readPersonMeasureByDates(2, "weight", new Date(2014-01-03), new Date(2014-8-03));
	for(HealthMeasureHistory history:histories)
	{
		System.out.println(history.getMid());
	}
	}
	
	
	/**
	 * Request #12,retrieve person list on a min and max range
	 */
	
	@Ignore
	@Test
	public void readStatusOnMinMaxValue()
	{
		List<Person>peoples=PeopleLogicImpl.readPersonListByMeasurement("weight","200","500");
		for(Person p:peoples)
		{
			System.out.println(p.getFirstname());
		}
	}
	@BeforeClass
	public static void beforeClass() {
	System.out.println("Testing JPA on lifecoach database using 'introsde-jpa' persistence unit");
	emf = Persistence.createEntityManagerFactory("SoapHealthServices");
	System.out.println("Created EMF is "+emf);
	em = emf.createEntityManager();
	System.out.println("Entity manager created++++++++++++++++++++++++++++++++++"+ em);
	}
	@AfterClass
	public static void afterClass() {
		try
		{
	em.close();
	emf.close();
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
	}
	@Before
	public void before() {
	tx = em.getTransaction();
	}
	private static EntityManagerFactory emf;
	private static EntityManager em;
	@SuppressWarnings("unused")
	private EntityTransaction tx;
}
